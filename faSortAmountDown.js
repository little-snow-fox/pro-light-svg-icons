'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fal';
var iconName = 'sort-amount-down';
var width = 512;
var height = 512;
var ligatures = [];
var unicode = 'f160';
var svgPathData = 'M204.485 392l-84 84.485c-4.686 4.686-12.284 4.686-16.971 0l-84-84.485c-4.686-4.686-4.686-12.284 0-16.97l7.07-7.071c4.686-4.686 12.284-4.686 16.971 0L95 419.887V44c0-6.627 5.373-12 12-12h10c6.627 0 12 5.373 12 12v375.887l51.444-51.928c4.686-4.686 12.284-4.686 16.971 0l7.07 7.071c4.687 4.686 4.687 12.284 0 16.97zM384 308v-8c0-6.627-5.373-12-12-12H268c-6.627 0-12 5.373-12 12v8c0 6.627 5.373 12 12 12h104c6.627 0 12-5.373 12-12zm64-96v-8c0-6.627-5.373-12-12-12H268c-6.627 0-12 5.373-12 12v8c0 6.627 5.373 12 12 12h168c6.627 0 12-5.373 12-12zm64-96v-8c0-6.627-5.373-12-12-12H268c-6.627 0-12 5.373-12 12v8c0 6.627 5.373 12 12 12h232c6.627 0 12-5.373 12-12zM320 404v-8c0-6.627-5.373-12-12-12h-40c-6.627 0-12 5.373-12 12v8c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12z';

exports.definition = {
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};

exports.faSortAmountDown = exports.definition;
exports.prefix = prefix;
exports.iconName = iconName;
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;