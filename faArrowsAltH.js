'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fal';
var iconName = 'arrows-alt-h';
var width = 512;
var height = 512;
var ligatures = [];
var unicode = 'f337';
var svgPathData = 'M384 192.032V239H128v-46.962c0-28.425-34.488-42.767-54.627-22.627l-64 63.962c-12.496 12.496-12.498 32.757 0 45.255l64 63.968C93.472 362.695 128 348.45 128 319.968V273h256v46.962c0 28.425 34.487 42.767 54.627 22.627l64-63.962c12.496-12.496 12.498-32.757 0-45.255l-64-63.968C418.528 149.305 384 163.55 384 192.032zM100 319.968c0 3.548-4.296 5.361-6.833 2.823l-63.995-63.963a3.995 3.995 0 0 1-.006-5.651l64.006-63.968c2.533-2.532 6.829-.724 6.829 2.828v127.931zm318.833-130.76l63.995 63.963a3.995 3.995 0 0 1 .006 5.651l-64.006 63.968c-2.532 2.532-6.829.725-6.829-2.828v-127.93c.001-3.548 4.297-5.361 6.834-2.824z';

exports.definition = {
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};

exports.faArrowsAltH = exports.definition;
exports.prefix = prefix;
exports.iconName = iconName;
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;