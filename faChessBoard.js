'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fal';
var iconName = 'chess-board';
var width = 512;
var height = 512;
var ligatures = [];
var unicode = 'f43c';
var svgPathData = 'M0 0v512h512V0H0zm480 480H32V32h448v448zm-352-32H64v-64h64v64zm64-64h64v64h-64v-64zm128 0h64v64h-64v-64zm-128 0h-64v-64h64v64zm128-64v64h-64v-64h64zm64 64v-64h64v64h-64zM128 256v64H64v-64h64zm128 64h-64v-64h64v64zm64-64h64v64h-64v-64zm-128 0h-64v-64h64v64zm64 0v-64h64v64h-64zm128 0v-64h64v64h-64zM128 128v64H64v-64h64zm64 64v-64h64v64h-64zm128 0v-64h64v64h-64zm-128-64h-64V64h64v64zm128 0h-64V64h64v64zm128 0h-64V64h64v64z';

exports.definition = {
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};

exports.faChessBoard = exports.definition;
exports.prefix = prefix;
exports.iconName = iconName;
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;