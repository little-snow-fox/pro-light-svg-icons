'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fal';
var iconName = 'align-center';
var width = 448;
var height = 512;
var ligatures = [];
var unicode = 'f037';
var svgPathData = 'M352 52v24a6 6 0 0 1-6 6H102a6 6 0 0 1-6-6V52a6 6 0 0 1 6-6h244a6 6 0 0 1 6 6zM6 210h436a6 6 0 0 0 6-6v-24a6 6 0 0 0-6-6H6a6 6 0 0 0-6 6v24a6 6 0 0 0 6 6zm0 256h436a6 6 0 0 0 6-6v-24a6 6 0 0 0-6-6H6a6 6 0 0 0-6 6v24a6 6 0 0 0 6 6zm340-164H102a6 6 0 0 0-6 6v24a6 6 0 0 0 6 6h244a6 6 0 0 0 6-6v-24a6 6 0 0 0-6-6z';

exports.definition = {
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};

exports.faAlignCenter = exports.definition;
exports.prefix = prefix;
exports.iconName = iconName;
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;