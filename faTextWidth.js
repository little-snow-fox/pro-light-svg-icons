'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
var prefix = 'fal';
var iconName = 'text-width';
var width = 448;
var height = 512;
var ligatures = [];
var unicode = 'f035';
var svgPathData = 'M0 116V44c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v72c0 6.627-5.373 12-12 12h-8.48c-6.627 0-12-5.373-12-12V64H240.556v256H284c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12H164c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h43.444V64H32.48v52c0 6.627-5.373 12-12 12H12c-6.627 0-12-5.373-12-12zm445.123 309.87l-48.001-39.984c-4.77-3.973-13.122-1.396-13.122 6.143V416H64v-23.998c0-6.841-7.971-10.434-13.122-6.143L2.877 425.843c-3.833 3.193-3.838 9.089 0 12.287l48.001 39.984C55.648 482.087 64 479.51 64 471.971V448h320v23.998c0 6.841 7.971 10.434 13.122 6.143l48.001-39.985c3.834-3.192 3.838-9.088 0-12.286z';

exports.definition = {
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};

exports.faTextWidth = exports.definition;
exports.prefix = prefix;
exports.iconName = iconName;
exports.width = width;
exports.height = height;
exports.ligatures = ligatures;
exports.unicode = unicode;
exports.svgPathData = svgPathData;